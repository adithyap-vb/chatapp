import logo from "./logo.svg";
import "./App.css";
import ChatWindow from "./ChatWindow.js";
import { ChakraProvider } from "@chakra-ui/react";
function App() {
  return (
    <ChakraProvider>
      <div className="App">
        <header className="titleBar">Chat</header>
        <ChatWindow />
      </div>
    </ChakraProvider>
  );
}

export default App;
