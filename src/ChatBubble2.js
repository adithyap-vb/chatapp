import "./ChatBubble2.css";

function ChatBubble2(props) {
  let dateNew = new Date(props.createdAt);
  //console.log(dateNew.getHours());
  //console.log("Created At" + props.createdAt);
  let res = timeSince(dateNew);
  //console.log("res is " + res);

  return (
    <div className="bubble2">
      <h2 className="msg2">{props.message}</h2>
      <h3 className="timeNew">{res}</h3>
    </div>
  );
}

function timeSince(date) {
  var seconds = Math.floor((new Date() - date) / 1000);

  var interval = seconds / 31536000;
  if (interval === 1) {
    return Math.floor(interval) + " year ago";
  }

  if (interval > 1) {
    return Math.floor(interval) + " years ago";
  }

  interval = seconds / 2592000;

  if (interval === 1) {
    return Math.floor(interval) + " month ago";
  }

  if (interval > 1) {
    return Math.floor(interval) + " months ago";
  }

  interval = seconds / 86400;
  if (interval === 1) {
    return Math.floor(interval) + " day ago";
  }

  if (interval > 1) {
    return Math.floor(interval) + " days ago";
  }

  interval = seconds / 3600;
  if (interval > 1) {
    return Math.floor(interval) + " hours ago";
  }
  interval = seconds / 60;
  if (interval > 1) {
    return Math.floor(interval) + " minutes ago";
  }
  return Math.floor(seconds) + " seconds ago";
}

export default ChatBubble2;
