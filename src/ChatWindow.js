import { useEffect, useState } from "react";
import { Input } from "@chakra-ui/react";
import { Button } from "@chakra-ui/button";
import ChatBubble from "./ChatBubble";
import ChatBubble2 from "./ChatBubble2";
import ChatBox from "./ChatBox";
const data = require("./data.js");
console.log("***");
function ChatWindow() {
  const [chat, setChat] = useState(data);
  //const [chatText, setChatText] = useState("");
  let chatText = "";
  const [placeholderText, setPlaceHolderText] = useState("Enter Chat");
  const [value, setValue] = useState("");

  const getTnput = (event) => {
    event.preventDefault();
    chatText = event.target.value;
  };

  const submitChat = (e) => {
    let chatObj = {
      message: chatText,
      createdAt: new Date().toISOString(),
      sender: "user",
      _id: chat.length + 1,
    };

    //  let temp = chat;
    //    temp.push(chatObj);
  };

  useEffect(() => {
    //alert("useEffect called");
  }, [JSON.stringify(chat)]);

  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          maxHeight: "500px",
          overflow: "auto",
        }}
      >
        {chat.map((item) => {
          if (item.sender === "user") {
            return (
              <div>
                <ChatBubble
                  message={item.message}
                  createdAt={item.createdAt}
                  sender={item.sender}
                />
              </div>
            );
          } else if (item.sender === "agent") {
            return (
              <ChatBubble2
                message={item.message}
                createdAt={item.createdAt}
                sender={item.sender}
              />
            );
          }
        })}
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          margin: "5px 10px 10px 5px",
        }}
      >
        <Input
          placeholder={placeholderText}
          variant="flushed"
          onChange={getTnput}
          id="input1"
          borderColor="#000000"
          backgroundColor="#fcecdd"
        />
        <Button
          placeholder="Send"
          style={{
            backgroundColor: "#FF710A",
            opacity: "80%",
          }}
          onClick={() => {
            let chatObj = {
              message: chatText,
              createdAt: new Date().toISOString(),
              sender: "user",
              _id: chat.length + 1,
            };

            setChat([...chat, chatObj]);
            setPlaceHolderText("Enter Chat");
            document.getElementById("input1").value = "";
          }}
        >
          Send
        </Button>
      </div>

      <div
        style={{
          display: "flex",
          flexDirection: "row",
          margin: "5px 10px 10px 5px",
        }}
      >
        <Input
          placeholder={placeholderText}
          variant="flushed"
          onChange={getTnput}
          id="input2"
          borderColor="#000000"
        />
        <Button
          placeholder="Send"
          style={{
            backgroundColor: "#FEA82f",

            opacity: "80%",
          }}
          onClick={() => {
            let chatObj = {
              message: chatText,
              createdAt: new Date().toISOString(),
              sender: "agent",
              _id: chat.length + 1,
            };

            setChat([...chat, chatObj]);
            setPlaceHolderText("Enter Chat");
            document.getElementById("input2").value = "";
          }}
        >
          Send
        </Button>
      </div>
    </div>
  );
}

export default ChatWindow;
